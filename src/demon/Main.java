package demon;

/**
 * JustMain)
 */
public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat("Кот", 54, 57);
        Dog dog = new Dog("Сабака", 76, 67);
        Hamster hamster = new Hamster("Хомяк", 75, 42);
        Pets[] animals = new Pets[]{cat, dog, hamster};

        for (int x = 8; x <= 22; x++) {
            System.out.println("Время " + x + ":00");
            for (int y = 0; y < 3; y++) {
                if (animals[y].getSatiety() < 25) {
                    animals[y].eat();
                    System.out.println(animals[y].getName() + " проголодался(лась) и я его покормил");
                } else if (animals[y].getFatigue() < 25) {
                    animals[y].sleep();
                    System.out.println(animals[y].getName() + " устал и я его положил(а) спать");
                } else {
                    animals[y].play();
                    System.out.println(animals[y].getName() + " хорошо провел(а) время");
                }


            }
        }
        mostHungry(animals);
    }

    /**
     * Метод вычисляющий самого голодного животного
     */
    public static void mostHungry(Pets[] animals) {
        if (animals[0].getSatiety() < animals[1].getSatiety() && animals[0].getSatiety() < animals[2].getSatiety()) {
            System.out.println("Самый голодный " + animals[0].toString());
        }
        if (animals[1].getSatiety() < animals[0].getSatiety() && animals[1].getSatiety() < animals[2].getSatiety()) {
            System.out.println("Самый голодный " + animals[1].toString());
        }
        if (animals[2].getSatiety() < animals[1].getSatiety() && animals[2].getSatiety() < animals[0].getSatiety()) {
            System.out.println("Самый голодный " + animals[2].toString());
        }
    }
}
